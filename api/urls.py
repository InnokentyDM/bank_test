from django.urls import path, include

from api.views import BanksList, BankDetail, ActionsList, ActionsDetail

urlpatterns = [
    path('users/', BanksList.as_view(), name='banks'),
    path('users/<int:pk>', BankDetail.as_view(), name='bank_detail'),
    path('actions/', ActionsList.as_view(), name='actions'),
    path('actions/<int:pk>', ActionsDetail.as_view(), name='action_detail'),
    path('/', include('rest_framework.urls', namespace='rest_framework'))
]


