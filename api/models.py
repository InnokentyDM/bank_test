from datetime import date

from django.db import models

# Create your models here.

class Actions(models.Model):
    shop = models.CharField(max_length=255, default=None)
    bank_id = models.IntegerField()
    last_update = models.DateTimeField(auto_now=True, verbose_name=u'Дата редактирования')

    def __str__(self):
        return '{}'.format(self.shop)


class ActionsManager(models.Manager):
    def shop(self, shop):
        today = date.today()
        actions_ids = Actions.objects.filter(shop=shop,
                                             last_update__day=today.day,
                                             last_update__month=today.month,
                                             last_update__year=today.year)
        banks_qs = Bank.objects.filter(ext_id__in=actions_ids)
        return banks_qs


class Bank(models.Model):
    name = models.CharField(max_length=255, default=None)
    ext_id = models.CharField(max_length=19, unique=True)

    def __str__(self):
        return '{}'.format(self.name)

    objects = models.Manager()
    actions = ActionsManager()
