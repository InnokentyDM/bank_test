from rest_framework import serializers

from api.models import Bank, Actions


class BankSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bank
        fields = ('id', 'name', 'ext_id')

class ActionsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Actions
        fields = ('id', 'shop', 'bank_id', 'last_update')