from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Bank, Actions
from api.serializers import BankSerializer, ActionsSerializer


class BanksList(generics.ListCreateAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer

class BankDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer



class ActionsList(generics.ListCreateAPIView):
    queryset = Actions.objects.all()
    serializer_class = ActionsSerializer

class ActionsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Actions.objects.all()
    serializer_class = ActionsSerializer