from django.contrib import admin

# Register your models here.
from api.models import Bank, Actions


@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    model = Bank


@admin.register(Actions)
class ActionsAdmin(admin.ModelAdmin):
    model = Actions